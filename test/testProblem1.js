const problem1 = require("../problem1.js");

problem1.createRandomDirectory()
.then(() => problem1.createRandomJsonfiles())
.then((fileName) =>problem1.deleteRandomFiles(fileName))
.then(()=> problem1.deleteDirectory())
.catch((err) => {
  console.error("Error:", err);
});