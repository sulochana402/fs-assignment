/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require("fs");
const path = require("path");

const randomDirectory = path.join(__dirname, "./randomFilesDirectory");

function createRandomDirectory() {
  return new Promise((resolve, reject) => {
    fs.mkdir(randomDirectory, { recursive: true }, (err) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        console.log("Created directory successfully");
        resolve();
      }
    });
  });
}

function createRandomJsonfiles() {
  return new Promise((resolve, reject) => {
    let randomFiles = Math.floor(Math.random() * 10) + 1;
    const createdFiles = [];

    const createFile = (index) => {
      if (index <= randomFiles) {
        const fileName = `fileName_${index}.json`;
        fs.writeFile(
          path.join(randomDirectory, fileName),
          JSON.stringify({ city: "Bangalore" }),
          (err) => {
            if (err) {
              reject(err);
            } else {
              console.log(`${fileName} created successfully`);
              createdFiles.push(fileName);
              if (createdFiles.length === randomFiles) {
                resolve(createdFiles);
              } else {
                createFile(index + 1);
              }
            }
          }
        );
      }
    };

    createFile(1);
  });
}

function deleteRandomFiles(files) {
  const deletePromises = files.map((fileName) => {
    return new Promise((resolve, reject) => {
      fs.unlink(path.join(randomDirectory, fileName), (err) => {
        if (err) {
          console.error(err);
          reject(err);
        } else {
          console.log(`${fileName} deleted successfully`);
          resolve();
        }
      });
    });
  });

  return Promise.all(deletePromises);
}

function deleteDirectory() {
  return new Promise((resolve, reject) => {
    fs.rmdir(path.join(randomDirectory), (err) => {
      if (err) {
        reject(err);
      } else {
        console.log("deleted  directory successfully");
        resolve();
      }
    });
  });
}

module.exports = {createRandomDirectory, createRandomJsonfiles, deleteRandomFiles, deleteDirectory};

