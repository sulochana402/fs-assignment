/*
    Problem 2: 
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs");
const path = require("path");

const inputFile = path.join(__dirname, "./lipsum.txt");
const uppercase = path.join(__dirname, "./uppercase.txt");
const lowercase = path.join(__dirname, "./lowercase.txt");
const sortedData = path.join(__dirname, "./sorted_content.txt");
const fileNames = path.join(__dirname, "./filenames.txt");

function readLipsumData() {
  return new Promise((resolve, reject) => {
    fs.readFile(inputFile, "utf-8", (err, data) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        console.log("Reading lipsum file");
        resolve(data);
      }
    });
  });
}

function convertToUppercase(data) {
  return new Promise((resolve, reject) => {
    let uppercaseData = data.toUpperCase();
    fs.writeFile(uppercase, uppercaseData, (err) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        console.log("Converted to uppercase data");
        writeFileName("uppercase.txt" + "\n")
          .then((value) => {
            console.log(value);
          })
          .catch((error) => {
            console.error(error);
          });
        resolve(uppercaseData);
      }
    });
  });
}

function writeFileName(data) {
  return new Promise((resolve, reject) => {
    fs.appendFile(fileNames, data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve("added file name");
      }
    });
  });
}
function convertToLowercase(data) {
  return new Promise((resolve, reject) => {
    let lowercaseData = data.toLowerCase().split(". ").join(".\n");
    fs.writeFile(lowercase, lowercaseData, (err) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        console.log("Converted to lowercase data");
        writeFileName("lowercase.txt" + "\n")
          .then((value) => {
            console.log(value);
            resolve(data);
          })
          .catch((error) => {
            console.error(error);
            reject(error);
          });
        resolve(lowercaseData);
      }
    });
  });
}

function sortedTheContent(data) {
  return new Promise((resolve, reject) => {
    const sortedsentence = data.split("\n").sort().slice(3).join("\n");
    fs.writeFile(sortedData, sortedsentence, (err) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        console.log("Sorted the sentences");
        writeFileName("sorted_content.txt" + "\n")
          .then((value) => {
            console.log(value);
            resolve(sortedsentence);
          })
          .catch((error) => {
            console.error(error);
            reject(error);
          });
      }
    });
  });
}

function readFilenamesFile() {
  return new Promise((resolve, reject) => {
    fs.readFile(fileNames, "utf-8", (err, data) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        console.log(`Reading ${fileNames}`);
        const fileNamesArray = data.trim().split("\n");
        resolve(fileNamesArray);
      }
    });
  });
}

function deleteFiles(files) {
  const deletePromises = files.map((fileName) => {
    return new Promise((resolve, reject) => {
      fs.unlink(fileName, (err) => {
        if (err) {
          console.error(`Error deleting file ${fileName}:`, err);
          reject(err);
        } else {
          console.log(`Deleted file ${fileName}`);
          resolve();
        }
      });
    });
  });

  return Promise.all(deletePromises);
}

function problem2() {
  return new Promise((resolve, reject) => {
    readLipsumData()
      .then((data) => convertToUppercase(data))
      .then((data) => convertToLowercase(data))
      .then((data) => sortedTheContent(data))
      .then(() => readFilenamesFile())
      .then((files) => deleteFiles(files))
      .then(() => {
        console.log("All files mentioned in filenames.txt have been deleted.");
        resolve("success");
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}

module.exports = problem2;
